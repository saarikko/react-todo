var React = require('react');
var ReactDOM = require('react-dom');
var expect = require('expect');
var $ = require('jQuery');
var TestUtils = require('react-addons-test-utils');

var TodoApp = require('TodoApp');

describe('TodoApp', () => {

	it('should exist', () => {
		expect(TodoApp).toExist();
	});

	it('should add tod to the todos on handleAddTodo', () => {
		var todoText = 'test';
		var todoApp = TestUtils.renderIntoDocument(<TodoApp/>);
		todoApp.setState({todos: []});
		todoApp.handleAddTodo(todoText);
		expect(todoApp.state.todos[0].text).toBe(todoText);
	});

	it('should toggle completed value when handdleToggle called', () => {
		var todo = {
			id: 22,
			text: 'test',
			completed: false
		}
		var todoApp = TestUtils.renderIntoDocument(<TodoApp/>);
		todoApp.setState({todos: [todo]});
		expect(todoApp.state.todos[0].completed).toBe(false);
		todoApp.handleToggle(22);
		expect(todoApp.state.todos[0].completed).toBe(true);
	});

});
