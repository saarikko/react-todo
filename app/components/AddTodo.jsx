var React = require('react');

var AddTodo = React.createClass({
	onSubmit: function(e) {
		e.preventDefault();
		var description = this.refs.description.value;
		if (description.length > 0) {
			this.refs.description.value = '';
			this.props.onAddTodo(description);
		} else {
			this.refs.description.focus();
		}
	},
	render: function() {
		return (
			<div>
				<form ref="form" onSubmit={this.onSubmit} className="add-todo-form">
					<input type="text" ref="description" placeholder="Add description"/>
					<button className="button expanded">Add Todo</button>
				</form>
			</div>
		);
	}
});

module.exports = AddTodo;